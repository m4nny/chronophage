from values import API_ADDRESS, API_AUTH
from utils import fetch_xml, flatten
from lxml import objectify
from copy import deepcopy


class Entity():
    """Abstract class of Trainee, Room and Instructor"""

    def __init__(self, xmlobj, categories):
        """Should not be accessed outside of the Entity object internals

        The user should use the static method Entity.fetchall() instead, which
        will create a list of all the Entity objects of a given type
        """

        self.idt = int(xmlobj.id)
        self.name = str(xmlobj.name)
        self.categories = deepcopy(categories)

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.idt == other.idt and self.name == other.name

    @classmethod
    def fetchall(cls, auth=API_AUTH):
        """Fetch all members of a given Entity, no arguments needed"""

        xml = fetch_xml(API_ADDRESS, {'auth': auth, 'getMenu': cls.title})
        return cls.__explore__(objectify.fromstring(xml))

    @classmethod
    def __explore__(cls, tree, cats=[]):
        """Should not be accessed outside of the Entity object internals"""

        if hasattr(tree, 'node'):
            return flatten(cls.__explore__(node, cats) for node in tree.node)
        elif hasattr(tree, 'nodes'):
            return cls.__explore__(tree.nodes, cats + [str(tree.name)])
        return cls(tree, cats)

    @classmethod
    def find(cls, terms, entities, strict=True, auth=API_AUTH):
        """Find a given entity identifier in the given Entity"""

        selectfunc = all if strict else any
        for ent in entities:
            if selectfunc(term.lower() in ent.name.lower() for term in terms):
                yield ent


class Trainee(Entity):

    title = 'trainnees'


class Room(Entity):

    title = 'rooms'


class Instructor(Entity):

    title = 'instructors'
